# Charts

These aren't actually charts but rather the values I use for the Helm charts on my home cluster.

## Helm Setup

```
helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
helm repo update
```

## Installing the charts

Be sure to update environment variables as needed, then run something like:

```
helm install nfs-client -f nfs.yaml nfs-subdir-external-provisioner/nfs-subdir-external-provisioner
```

## Updating with environment variables

```
export $(cat .env | xargs)
envsubst < atlantis.yaml | tee atlantis.yaml
```

## Gitlab Installed Apps

```
helm upgrade prometheus stable/prometheus --install --reset-values --tls --tls-ca-cert /data/helm/prometheus/config/ca.pem --tls-cert /data/helm/prometheus/config/cert.pem --tls-key /data/helm/prometheus/config/key.pem --version 6.7.3 --set 'rbac.create=true,rbac.enabled=true' --namespace gitlab-managed-apps -f /data/helm/prometheus/config/values.yaml
```

errors with:

```
Error: validation failed: unable to recognize "": no matches for kind "Deployment" in version "extensions/v1beta1"
```

This is directly related to [the bug](https://github.com/helm/charts/issues/19341) I reported.

Container image: `registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/2.16.1-kube-1.13.12`
